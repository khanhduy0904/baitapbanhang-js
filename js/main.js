// Phần 1: Giao diện

var productList = [];
var cartList = [];
// function 1: Lấy danh sách dt từ api
const fetchData = function () {
  // axios({
  //   url: "../DeThiTracNghiem.json",
  //   method: "GET",
  // })
  getProduct()
    .then(function (res) {
      console.log(res);
      mapData(res.data);
      //questionList = res.data;
      // render câu hỏi ra màn hình
      renderProduct(productList);
      console.log(productList);
      console.log(typeof productList[0].id);
    })
    .catch(function (err) {
      console.log(err);
    });
};

// function 2:Hiển thị từng sản phẩm dt ra màn hình
const renderProduct = function (arr) {
  let htmlContent = "";
  for (let i in arr) {
    htmlContent += arr[i].render();
  }
  document.getElementById("divPhoneItem").innerHTML = htmlContent;
};
// function 3: Map ds điện thoại của api sang ds điện thoại của mình(có thêm phương thức render)
const mapData = function (data) {
  for (let items of data) {
    var myProductObject;
    myProductObject = new Product(
      //items.id,
      items.name,
      items.image,
      items.description,
      items.price,
      items.inventory,
      items.rating,
      items.type
    );
    myProductObject = { id: items.id, ...myProductObject };
    productList.push(myProductObject);
  }
};

// Câu 1.1 function 4: Sort A-Z & Z-A
const sortProduct = function () {
  var value = document.getElementById("sortID").value;
  if (value === "AZ") {
    productList.sort(function (a, b) {
      var name1 = a.name.toLowerCase();
      var name2 = b.name.toLowerCase();
      if (name1 < name2) {
        return -1;
      }
      if (name1 > name2) {
        return 1;
      }
      return 0;
    });
  } else if (value === "ZA") {
    productList.reverse();
  }

  console.log(productList);
  renderProduct(productList);
};

// Câu 2 : Filter theo loại sản phẩm
// function 5.
const filterFromType = function () {
  let htmlContent;
  const foundSamsung = [];
  const foundIphone = [];
  var value = document.getElementById("fitterID").value;
  for (var i = 0; i < productList.length; i++) {
    const currentProduct = productList[i];
    var phoneType = currentProduct.type;
    phoneType = phoneType.toLowerCase();

    if (phoneType === "samsung") {
      foundSamsung.push(currentProduct);
      // renderProduct(foundSamsung);
    } else if (phoneType === "iphone") {
      foundIphone.push(currentProduct);
      //renderProduct(foundIphone);
    }
  }
  if (value === "Iphone") {
    renderProduct(foundIphone);
  } else if (value === "Samsung") {
    renderProduct(foundSamsung);
  }

  console.log(foundSamsung);
  console.log(foundIphone);
};
const renderArr = function (arr) {
  let htmlContent;
  for (let i in arr) {
    htmlContent += arr[i].render();
  }
  document.getElementById("divPhoneItem").innerHTML = htmlContent;
};

//Câu 3. Thêm vào giỏ hàng

// const findById = function (id) {
//   for (var i = 0; i < productList.length; i++) {
//     if (productList[i].id === id) {
//       return i;
//     }
//   }
//   return -1;
// };
// Câu 4-5. Add product có quantity vô cartList(mảng giỏ hàng)
const addProductToCard = function (productId) {
  //var index = findById(id);
  for (let i in productList) {
    let currentProduct = productList[i];

    if (currentProduct.id === productId) {
      let newProduct = { ...currentProduct, quantity: "1" };
      for (let i in cartList) {
        if (cartList[i].id === newProduct.id) {
          cartList[i].quantity++;
          renderCart();
          return;
        }
      }
      console.log(newProduct);
      cartList.push(newProduct);
    }
    //console.log(productList[i].id);
  }
  console.log(cartList);
  savaData();
  renderCart();
};

const renderCart = function () {
  let htmlContentTotal = "";
  let total = 0;
  let htmlContentCard = "";

  if (cartList.length === 0) {
    document.getElementById("tbodyCart").innerHTML = "";
    document.getElementById("total").innerHTML = "";
  } else {
    for (let i in cartList) {
      htmlContentCard += `
        <tr>
          <td><img style="height: 150px;"
          src="${cartList[i].image}"></td>
          <td style="font-size:25px;">${cartList[i].name}</td>
          <td>${cartList[i].price}</td>
          <td>
          ${cartList[i].quantity}
          <div class="btn-group">
            <button class="btn btn-info border-right" onclick="numberSubtract('${
              cartList[i].id
            }')">-</button>
            <button class="btn btn-info border-left" onclick="numberAdd('${
              cartList[i].id
            }')">+</button>
          </div>
          </td>
          <td>${cartList[i].price * cartList[i].quantity}</td>
          <td>
            <button class="btn btn-info" onclick="removeCartProduct(${
              cartList[i].id
            })">x</button>
          </td>
        </tr>
       
      `;
      total += cartList[i].price * cartList[i].quantity;
      // total =  new Intl.NumberFormat('en-IN').format(total);
      htmlContentTotal = `
        <span style="font-size:30px" class="font-weight-bold">Tổng Tiền: ${total} VND</span>
        <button style="font-size:30px" class="btn btn-info ml-5" onclick="removeCart()" >Thanh Toán</button>
      `;

      document.getElementById("total").innerHTML = htmlContentTotal;
      document.getElementById("tbodyCart").innerHTML = htmlContentCard;
      //savaData();
    }
  }
};

// Câu 6
const numberAdd = function (id) {
  for (let i in cartList) {
    if (cartList[i].id === id) {
      cartList[i].quantity++;
      savaData();
      renderCart();
    }
  }
};
const numberSubtract = function (id) {
  for (let i in cartList) {
    let currentCart = cartList[i];
    if (currentCart.id === id) {
      if (currentCart.quantity > 1) {
        currentCart.quantity--;
        savaData();
        renderCart();
      } else {
        return;
      }
    }
  }
};
// Câu 8. Xóa cartList khi thanh toán
const removeCart = function () {
  cartList = [];
  document.getElementById("tbodyCart").innerHTML = "";
  document.getElementById("total").innerHTML = "";
  renderCart();
  savaData();
  alert("Bạn đã thanh toán thành công");
  console.log(cartList);
};
// Câu 9
const savaData = function () {
  localStorage.setItem("cartList", JSON.stringify(cartList));
  //JSON.stringify: Chuyển mảng thành chuỗi
};

const getData = function () {
  var cartListJSON = localStorage.getItem("cartList");
  //Kiểm tra tồn tại dữ liệu
  if (!cartListJSON) {
    return;
  }
  // map từ mảng cũ [n1,n2,n3] => [new Empl(n1), new Empl(n2), new Empl(n3)]
  const cartListfromLocal = JSON.parse(cartListJSON);
  for (let i in cartListfromLocal) {
    let currentCartList = cartListfromLocal[i];
    let newCartList = { ...currentCartList };
    cartList.push(newCartList);
  }
  // for(let i=0; i<cartListfromLocal.length;i++){
  //   let currentCartList = cartListfromLocal[i];

  // }

  renderCart();
};

// Câu 10. Remove sản phẩm trong giỏ hàng
const removeCartProduct = function (cartId) {
  let index = findById(cartId);
  if (index !== -1) {
    cartList.splice(index, 1);
    console.log("run");
    savaData();
    renderCart();
  }
  console.log(cartList);
};

const findById = function (id) {
  for (var i = 0; i < cartList.length; i++) {
    // cartList[i].id = cartList[i].id*1;
    if (cartList[i].id * 1 === id) {
      return i;
    }
  }
  return -1;
};

getData();

fetchData();

// Phần 2.
// Câu 1. Thêm product
// document
//   .getElementById("btn-addProduct")
//   .addEventListener("click", openaddProduct);

// function openaddProduct() {
//   document.getElementsByClassName("modal-footer")[0].innerHTML = `
//     <button class="btn btn-success" onclick=" addNewProduct()">Thêm</button>
//   `;
// }

// function addNewProduct() {
//   const newimage = document.getElementById("image").value;
//   const name = document.getElementById("name").value;
//   const description = document.getElementById("description").value;
//   const price = document.getElementById("price").value;
//   const inventory = document.getElementById("inventory").value;
//   const rating = document.getElementById("rating").value;
//   const type = document.getElementById("type").value;

//   const newProduct = new Product(
//     name,
//     newimage,
//     description,
//     price,
//     inventory,
//     rating,
//     type
//   );
//   addProduct(newProduct).then(function (result) {
//     fetchData();
//     //renderProduct(productList);
//   });
// }
